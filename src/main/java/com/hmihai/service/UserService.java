package com.hmihai.service;

import com.hmihai.model.User;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by evo on 11/23/2017.
 */

@Service
public class UserService {
    private List<User> users = new ArrayList<User>();

    UserService(){
        User user = new User();
        user.setEmail("johndoe123@gmail.com");
        user.setName("John Doe");
        user.setAddress("Bangalore, Karnataka");
        User user1 = new User();
        user1.setEmail("amitsingh@yahoo.com");
        user1.setName("Amit Singh");
        user1.setAddress("Chennai, Tamilnadu");
        User user2 = new User();
        user2.setEmail("bipulkumar@gmail.com");
        user2.setName("Bipul Kumar");
        user2.setAddress("Bangalore, Karnataka");
        User user3 = new User();
        user3.setEmail("prakashranjan@gmail.com");
        user3.setName("Prakash Ranjan");
        user3.setAddress("Chennai, Tamilnadu");
        users.add(user1);
        users.add(user2);
        users.add(user3);
    }

    public void addUser(User user){
        users.add(user);
    }

    public List<User> findAll(){
        return users;
    }


}
