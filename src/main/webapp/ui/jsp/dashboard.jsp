<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Spring Security Example</title>
    <link href="/bootstrap.min.css" rel="stylesheet">
    <script src="/jquery-2.2.1.min.js"></script>
    <script src="/bootstrap.min.js"></script>
</head>
<body>
<div>
    <div class="container" style="margin: 50px">

        <div>
            <form action="/logout" method="post">
                <button type="submit" class="btn btn-danger">Log Out</button>
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>
            </form>
        </div>

        <div>
            <form:form class="form-inline" style="margin:20px 20px 20px 20px" id="customerForm" action="addUser" method="POST" commandName="user">
                <div class="form-group">
                    <form:label for="name" style="margin-right:5px" path="name">FirstName:</form:label>
                    <form:input type="text" class="form-control" id="name" placeholder="Enter Name" path="name"/>
                </div>
                <div class="form-group">
                    <form:label for="email" style="margin-left:20px; margin-right:5px" path="email">LastName:</form:label>
                    <form:input type="text" class="form-control" id="email" placeholder="Enter Email" path="email"/>
                </div>
                <div class="form-group">
                    <form:label for="address" style="margin-left:20px; margin-right:5px" path="address">LastName:</form:label>
                    <form:input type="text" class="form-control" id="address" placeholder="Enter Address" path="address"/>
                </div>
                <button type="submit" class="btn btn-default" style="margin-left:20px; margin-right:5px">Submit</button>
            </form:form>
        </div>

        <div class="row text-center">
            <strong> User Details</strong>
        </div>
        <div class="row" style="border: 1px solid green; padding: 10px">
            <div class="col-md-4 text-center">
                <strong>Name</strong>
            </div>
            <div class="col-md-4 text-center">
                <strong>Email</strong>
            </div>
            <div class="col-md-4 text-center">
                <strong>Address</strong>
            </div>
        </div>

        <c:forEach var="user" items="${users}">
            <div class="row" style="border: 1px solid green; padding: 10px">
                <div class="col-md-4 text-center">${user.name}</div>
                <div class="col-md-4 text-center">${user.email}</div>
                <div class="col-md-4 text-center">${user.address}</div>
            </div>
        </c:forEach>

    </div>
</div>
</body>
</html>