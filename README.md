# Spring Web Application

## Description 

Sample web application including login, logout, post form and list view.  

## Technology stack

* Spring Security
* Spring MVC 
* Spring Forms
* Bootstrap 
* JQuery 

## Build and Run
 
 mvn spring-boot:run 
 
Default user and password: john123/password (can be changed from SpringSecurityConfig)

